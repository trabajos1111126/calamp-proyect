import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Main {
  public Main() {
  }

  public static void main(String[] args) {
    new Scanner(System.in);
    String SERVER_ADDRESS = "127.0.0.1";//"127.0.0.1";//64.225.5.133//127.0.0.1
    int SERVER_PORT = 20500;//20500;

    try {
      Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);

      try {
        (new Thread(new ReceivedMessagesHandler(socket.getInputStream()))).start();
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in));

        while(true) {
          System.out.print("> ");
          String message = keyboardReader.readLine();
          if (message.equalsIgnoreCase("exit")) {
            break;
          }

          out.println(message);
        }
      } catch (Throwable var9) {
        try {
          socket.close();
        } catch (Throwable var8) {
          var9.addSuppressed(var8);
        }

        throw var9;
      }
    } catch (IOException var10) {
      IOException e = var10;
      e.printStackTrace();
    }
  }

  private static class ReceivedMessagesHandler implements Runnable {
    private InputStream server;

    public ReceivedMessagesHandler(InputStream server) {
      this.server = server;
    }

    public void run() {
      try {
        BufferedReader reader = new BufferedReader(new InputStreamReader(this.server));

        String message;
        try {
          while((message = reader.readLine()) != null) {
            System.out.println("Received: " + message);
            System.out.print("> ");
          }
        } catch (Throwable var5) {
          try {
            reader.close();
          } catch (Throwable var4) {
            var5.addSuppressed(var4);
          }

          throw var5;
        }

        reader.close();
      } catch (IOException var6) {
        IOException e = var6;
        e.printStackTrace();
      }
    }
  }
}
