import java.io.IOException;
import java.math.BigInteger;
import java.net.*;
import java.nio.charset.StandardCharsets;

public class Main {
  private static volatile boolean running = true;

  public static void main(String[] args) {

    //System.out.println(stringToHex(""));
    DatagramSocket socket = null;
    try {
      // Crear el socket UDP
      socket = new DatagramSocket();

      // Crear un hilo para manejar los mensajes recibidos
      Thread receivedMessagesHandler = new Thread(new ReceivedMessagesHandler(socket));
      receivedMessagesHandler.start();

      // Leer mensaje desde el teclado
      System.out.print("> ");
      // Mensaje en texto
      String message = "83054634005461010101020001661997DB00000000000000000000000000000000000000000000002C0000FF8F00001E0826F4100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";

      // Convertir el mensaje a bytes
      //byte[] messageBytes = message.getBytes(StandardCharsets.UTF_8);

      // Convertir los bytes a una cadena hexadecimal
      //String hexMessage = byteArrayToHexString(message);

      // Convertir la cadena hexadecimal a bytes
      byte[] bytes = hexStringToByteArray(message);
      // Crear un paquete con los datos y la dirección IP y puerto del servidor
      DatagramPacket sendPacket = new DatagramPacket(bytes, bytes.length,
          InetAddress.getByName("127.0.0.1"), 80);
      //InetAddress.getByName("64.225.5.133"), 20500);
              //InetAddress.getByName("35.243.229.128"), 20500);

      // Enviar el paquete al servidor
      socket.send(sendPacket);


      // Esperar un poco antes de cerrar el socket
      Thread.sleep(1000);

    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
    } finally {
      // Detener el hilo de manejo de mensajes recibidos antes de cerrar el socket
      running = false;

      // Cerrar el socket y liberar los recursos
      if (socket != null) {
        socket.close();
      }
    }
  }

  // Función para convertir una cadena hexadecimal en un array de bytes
  public static byte[] hexStringToByteArray(String hexString) {
    int len = hexString.length();
    byte[] data = new byte[len / 2];
    for (int i = 0; i < len; i += 2) {
      data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
              + Character.digit(hexString.charAt(i+1), 16));
    }
    return data;
  }

  // Función para convertir un array de bytes en una cadena hexadecimal
  public static String byteArrayToHexString(byte[] bytes) {
    StringBuilder sb = new StringBuilder();
    for (byte b : bytes) {
      sb.append(String.format("%02X", b));
    }
    return sb.toString();
  }

  private static class ReceivedMessagesHandler implements Runnable {
    private DatagramSocket socket;

    public ReceivedMessagesHandler(DatagramSocket socket) {
      this.socket = socket;
    }

    @Override
    public void run() {
      try {
        byte[] receiveData = new byte[1024];
        while (running) { // Utilizar la bandera "running" para controlar la ejecución del hilo
          // Preparar para recibir un paquete
          DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

          // Recibir el paquete del servidor
          socket.receive(receivePacket);

          // Convertir los bytes recibidos en un String y mostrarlo en la consola
          String receivedMessage = new String(receivePacket.getData(), 0, receivePacket.getLength());
          System.out.println("Received: " + receivedMessage);
          System.out.print("> ");
        }
      } catch (IOException e) {
        // Ignorar excepciones cuando se cierra el socket
        if (running) {
          e.printStackTrace();
        }
      }
    }
  }
}
