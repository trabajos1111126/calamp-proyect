import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
  public static void main(String[] args) {
    final int PORT = 80;//20500;

    try {
      // Crear un ServerSocket en el puerto especificado
      ServerSocket serverSocket = new ServerSocket(PORT);
      System.out.println("Servidor TCP iniciado en el puerto " + PORT);

      // Esperar a que lleguen conexiones de clientes
      while (true) {
        // Aceptar la conexión del cliente
        Socket clientSocket = serverSocket.accept();
        System.out.println("Cliente conectado desde " + clientSocket.getInetAddress().getHostAddress());

        // Crear un hilo para manejar la comunicación con el cliente
        Thread clientThread = new Thread(new ClientHandler(clientSocket));
        clientThread.start();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static class ClientHandler implements Runnable {
    private Socket clientSocket;

    public ClientHandler(Socket clientSocket) {
      this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
      try {
        // Crear canales de entrada y salida para la comunicación con el cliente
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

        // Leer mensajes del cliente y responder
        String message;
        while ((message = in.readLine()) != null) {
          System.out.println("Mensaje recibido del cliente: " + message);
          out.println("Mensaje recibido: " + message);
        }

        // Cerrar los canales y el socket del cliente
        in.close();
        out.close();
        clientSocket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
