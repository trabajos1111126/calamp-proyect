# calamp proyect

Adjuntamos ejemplos de cliente y servidor para verificar la coneccion de local por udp y tcp

## Getting started

Este proyecto esta destinado a la codificacion de dispositivos calamp de la version lmu 26XX y LMU 27XX

## Descripcion de leds

Los led que tiene le dispositivo son 2 uno verde y otro naranjado
sirven para verificar la conección 

- VERDE: Estado del GPS
- NARANJA: Estado de la seña movil.

para su coneccion  los leds tiene que estar en luz fija es decir sin parpadear

<div align="center">
  <img src="leds.jpg" alt="Logo de Mi Proyecto" width="1000">
</div>

## Codigos SMS

Estos son algunos de los codigos que responde el dispositivo GPS al acceder por sms

- !R3,70,0				Resetea el dispositivo
- !R3,37,0  			Reinicia o enciende el GPRS
- !R0		  			Estado Actual del dispositivo
- !R4					Trama de datos encriptado
- !R5					Velocidad y ubicacion del dispositivo
- !R9                 	Actualizacion de los comandos
- !R9,0                 Desactivar actualizaciones automáticas(Sin Probar)
- !R3,37,1              Cambiar a modo autónomo (Standalone) (Sin Probar)
- !RJ                 	Te da el link de la ubicación
- !RG					ubicacion lat long altitud

- !RC                   Reporte de econfig de dispositivo

---------------------------------------------------

- !RP?2306,0			Consulta la VPN actual

- !RP,2306,0,4g.entel Colocar VPN

---------------------------------------------------

- !RP,768,0,ddd.ddd.ddd.ddd		Donde ddd.ddd.ddd.ddd es la dirección IPV4 direccionable públicamente del servidor
								en este caso de ende estamos apuntando a  31.961.671.53
- !RP?768,0			verificamos donde se enviaran los datos

- !RP?769,0 			Verificamos que puerto usaremos   //!R1,267,0, puerto Configuro puerto

- !RP,769,0,<puerto>	Agregamos puerto UDP configurado

- !RP,1024,35,255,1		Usamos para guardar cambios despues tenemos que colocar !R3,70,0 para verificar los cambios

---------------------------------------------------

- !RP,2319,0,<ip>		Agregamos a que IP enviaremos los datos "www.google.com"

- !RP?2319,0 			Verificamos que IP usaremos

---------------------------------------------------

En nuestro caso el APM no necesita un usuario ni contraseña
en algunos dispositivos esta configurado el user y contraseña como "dummy"

- !RP?2314,0			Verificamos el Usuario

- !RP,2314,0,{Nombre de usuario del APN}

---------------------------------------------------

- !RP?2315,0			Verificamos la contraseña del APN

- !RP,2315,0,{Contraseña del APN}

- !R3,49,129            descarga las configuraciones

- !R3,5,0               Borra Historial de Logs

- !R3,44,255            XXX posiblemente para forzar a enviar datos

---------------------------------------------------


CAMBIAR FRECUENCIA DE REPORTE

----CUANDO EL VEHICULO ESTA ENCENDIDO

Para 24 horas: !R1,265,0,86400

Para 48 horas: !R1,265,0,172800

Para 72 horas: !R1,265,0,259200

----CUANDO EL VEHICULO ESTA APAGADO

Para 24 horas: !R1,267,0,86400

Para 48 horas: !R1,267,0,172800

Para 72 horas: !R1,267,0,259200

Otro codigo para probar 

 !RP,262,0,60  (Define el valor del informe por tiempo)


---------------------------------------------------

## Pasos

1 Colocamos la ip del backend  con !RP,2319,0,"ejemplo.backend" en el destino 0,1,2

2 Colocamos puerto !RP,769,0,<puerto> en el destino 0,1,2

3 Guardamos cambios de el dispositivo !RP,1024,35,255,1

4 Reiniciamos el dispositivo !R3,70,0

5 consultamos la respuesta !RC

```

```

## Ejemplo de comentario de decodificacion de Trama

encontre esta publicacion Por si sirve de algo, al enviar un comando !R4 por SMS al dispositivo, recuperé esto por SMS...


```

!E4572035111FFFFFF010000654FBC1F654FBC1F171273FEB7C4B40E000002F000F80A000104FFBBAF091300FFFF0400003570000002D200000000000001064
```

El cual se decodifica perfectamente con toda la información válida del programa CalAmp LMDecoder.exe.

```
MobileID 4572035111FFFFFF
MobileIDType ESN
Sequence# 0
UpdateTime 11/11/2023 5:38:39 PM
TimeOfFix 11/11/2023 5:38:39 PM
Latitud 38.708531
Longitud -121.1845618
Velocidad 16.8 mph
Rumbo suroeste (248 grados)
Satélites 10
Fix Estado Buen
Transportista 260
RSSI Muy bueno (-69)
Estado de comunicación disponible, red, datos, conectado, roaming
HDOP ideal (.9)
Entradas IgnitionOn, Entrada1, Entrada4
Estado de unidad Bueno
EventIndex Solicitud de PEG en tiempo real
Código de evento Solicitud de PEG en tiempo real
Conteo acumulado 4
Repuesto B7
Acumulación 0 00003570
Acumulación 1 000002D2
Acumulación 2 00000000
Acumulación 3 00001064
```
Entonces, no estoy seguro de por qué el formato SMS parece legítimo, pero el informe a traccar parece

- [ ] [Fuente] (https://www.traccar.org/forums/topic/calamp-lmu3030/)

# Respuesta !R0 Reporte de gps

ejemplo de respuesta 

```
APP:081 8.3d
COM:0
GPS:No Time Sync
INP:11100111 13.7V
MID:4141000100 ESN
INB:207.7.101.227:20500 LMD
```

# PULS 
PULS™ (Sistema de Programación, Actualización y Logística) es el servidor de mantenimiento basado en web de CalAmp que ofrece configuración sin intervención y actualizaciones automáticas post-instalación. PULS™ proporciona un medio para actualizar parámetros de configuración, scripts PEG y firmware a través del aire (OTA, por sus siglas en inglés) y permite a los clientes de CalAmp monitorear el estado de salud de las unidades en las flotas de tus clientes para identificar rápidamente problemas antes de que se conviertan en problemas costosos.

# Verificacion via sms 
El estado actual de la comunicación, GPS y de entrada de un GSM LMU se puede obtener a través de SMS siempre que tengas acceso a un teléfono o PDA capaz de enviar mensajes de texto. Utilizando tu dispositivo móvil, envía el siguiente mensaje de texto al LMU:
```
!R0
```
En unos pocos minutos, el LMU debería devolver una respuesta en el siguiente formato:
```
APP: <App ID> <Firmware Version>
COM:<RSSI> [./d/D][./a/A][./L][IP address] [<APN>]
GPS:[Antenna <Short/Open/Off>] | [No Time Sync] | [<FixStatus> <SatCount>]
INP:<inputs states> <vehicle voltage>
MID:<mobile ID> <mobile ID type>
INB:<inbound IP address>:<inbound port> <Inbound Protocol (LMD/LMX)>
```
- APP
  - App ID: El valor de Identificación de Aplicación del LMU indica la plataforma host y la tecnología de red inalámbrica del LMU.
  - Firmware Version: La versión actual del firmware en uso por el LMU.
- COM
  - RSSI: Esta es la intensidad de la señal que el módem inalámbrico detecta desde la red. En general, el LMU está al menos escaneando la red si el RSSI no es -113.
  - [./d/D]: Si el carácter 'D' está presente, indica que el LMU tenía una sesión de datos establecida cuando respondió a la solicitud de estado. Para la línea de productos de 8 bits, una 'D' en mayúscula indica que tanto los sockets de entrada como de mantenimiento están listos. La 'd' en minúscula indica que solo el socket de mantenimiento está listo. Un '.' indica que no hay sockets listos.
  - [./a/A]: Este campo indica si el LMU ha recibido un Acuse de Recibo (ACK) del servidor de entrada. Este campo estará vacío si el LMU nunca ha recibido un ACK. La letra minúscula 'a' estará presente si ha recibido un ACK desde el último reinicio en frío (es decir, ciclo de encendido) pero no desde el último reinicio en caliente (Reinicio de Aplicación o Reposo). La letra mayúscula 'A' estará presente si el LMU ha recibido un ACK desde el último reinicio en caliente. Un '.' indica que no se ha recibido ningún acuse de recibo.
  - [./L]: Este campo indica si el registro del LMU está actualmente activo. Una 'L' indica que el registro está en uso (es decir, se ha almacenado uno o más registros), mientras que un '.' indica que el registro está inactivo.
  - [IP address] Este es un campo opcional y solo está presente si el LMU ha establecido una sesión de datos válida. Este campo contendrá la dirección IP actual del LMU asignada por la red inalámbrica. Ten en cuenta que si ves un valor de 192.168.0.0, esto indica que el LMU no ha podido establecer una sesión de datos.
  - [APN] El Nombre del Punto de Acceso actualmente en uso por un GSM LMU.
- GPS
  - Antenna Short/Open/Off: Este campo, si está presente, indica un problema con la antena GPS del LMU. Un valor de "Short" indica que es probable que el cable de la antena haya sido aplastado. Un valor de "Open" indica que el cable de la antena está cortado o desconectado. Un valor de "Off" indica que el receptor GPS del LMU está apagado.
  - No Time Sync: Si este campo está presente, indica que el receptor GPS del LMU no ha podido encontrar ni siquiera un solo satélite GPS. Esto probablemente se vería en conjunto con el error de antena mencionado anteriormente, o si la antena GPS del LMU está bloqueada de alguna manera.
  - <FixStatus> <SatCount>: Si estos campos están presentes, indica que el LMU tiene o tuvo una solución GPS válida. El campo <Sat Count> indica cuántos satélites GPS están actualmente en uso por el LMU. El campo <FixStatus> indica el tipo de solución. Los tipos de estado de solución están detallados en **LMU reference Guide**
- INP
  - inputs states: This field details the current state of each of the LMU's discreet inputs. This field is always 8characters long. The left most character represents the state of input 7 where the right most representsthe state of input 0 (i.e. the ignition). A value of 1 indicates the input is currently in the high state. Avalue of 0 indicates it is currently in the low state.
  - vehicle voltage: Este campo contendrá la lectura actual del conversor analógico-digital (A/D) interno del LMU. Esto será el voltaje de suministro proporcionado al LMU en mV (milivoltios).
- MID
  - mobile ID: Este será el ID móvil actualmente utilizado por el LMU.
  - mobile ID type: Este será el tipo de ID móvil en uso por el LMU. Los tipos disponibles son: Off, ESN, IMEI, IMSI, USER, MIN e IP ADDRESS.
- INB
  - inbound IP address: Esta es la dirección IP actualmente utilizada por el LMU. Este valor debe coincidir con la dirección IP de tu servidor LMDirect™.
  - inbound port: Este es el puerto UDP actual que utilizará el LMU para entregar sus datos de LM Direct™. Este valor debe coincidir con el puerto UDP que estás utilizando en tu servidor LM Direct™. Normalmente es el puerto 20500
  - Inbound Protocol (LMD/LMX): Este es el protocolo de mensajería UDP/IP actualmente utilizado por el LMU. En general, debería ser LMD. Ejemplo de respuesta GSM.



## drive de material de apoyo

- [ ] [Descripcion  de respuestas sms] (https://content.gps-trace.com/assets/7509410d-9804-49fa-9af7-1f561754adc5)
- [ ] [Repositorio Drive](https://drive.google.com/drive/u/0/folders/1NhAE7N2BaYf8Y5rX8lOLlD5Ruhj65IOb)
- [ ] [Comandos mas Utiles] (https://support.hologram.io/hc/en-us/articles/360035696993-CalAmp-LMU-3030)
- [ ] [Libro mas Util Pag.50] (https://content.gps-trace.com/assets/7509410d-9804-49fa-9af7-1f561754adc5)
- [ ] [Coandos en portuges] (https://wiki.smartgps.com.br/books/calamp/page/comandos)
- [ ] [Descripcion de datos de respuesta] (https://app-direct-www-cloudfront.s3.amazonaws.com/downloads/194004/CalAmp-LMU-Series-Protocol-Adapter-SDK-Setup-Guide.pdf)
- [ ] [manual de instalación LMU-3030] (https://www.manualslib.com/manual/1282093/Calamp-Lmu-3030.html?page=28#manual)
- [ ] [Manual de instalacion LMU-2600] (https://www.manualslib.com/manual/3225025/Calamp-Lmu-2600.html?page=3#manual)
- [ ] [Proyecto a base de calamp y tracar] (https://github.com/edsfocci/Transitime_core/blob/master/transitime/src/main/java/org/transitime/avl/calAmp/MiniEventReport.java)
- [ ] [Paguina para verificar coneccion TCP y UDP] (https://check-host.net/)
	https://smartlocaliza.com.br/configuracoes/calamp/calamp/
	https://localizerastreios.com/clientes/knowledge-base/article/configura-oes-do-rastreador-calamp
## Collaborate with your team 
