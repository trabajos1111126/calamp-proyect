import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.time.LocalTime;


public class Main {
    public static void main(String[] args) {
        DatagramSocket socket = null;
        final int PORT = 80;//20500;
        try {
            socket = new DatagramSocket(PORT);
            System.out.println("Servidor UDP iniciado en el puerto interno" + PORT + "...");

            byte[] receiveData = new byte[256];

            while (true) {
                // Preparar para recibir un paquete
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                socket.receive(receivePacket);
                parseReport(receivePacket);
                System.out.println(LocalTime.now());
                System.out.println("------------------------------");
                // (Opcional) - Responder al cliente
                String responseMessage = "Respuesta desde el servidor UDP";
                byte[] sendData = responseMessage.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, receivePacket.getAddress(), receivePacket.getPort());
                socket.send(sendPacket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    public static void parseReport(DatagramPacket packet) {
        byte[] bytes = packet.getData();
        // Log the entire message in hexadecimal format
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < packet.getLength(); ++i) {
            sb.append(String.format("%02X", bytes[i]));
        }
        System.out.println("Message={}" + sb.toString());
        // Didn't successfully create a report so return null
        // Read options header
        OptionsHeader optionsHeader = OptionsHeader.getOptionsHeader(bytes);
        int messageStartIdx =
                optionsHeader != null ? optionsHeader.getNextPart() : 0;
        System.out.println("Options header {}" + optionsHeader);

        // Read message header, which specifies type of report
        MessageHeader messageHeader =
                MessageHeader.getMessageHeader(bytes, messageStartIdx);
        System.out.println("Message header {}" + messageHeader);
    }
}
